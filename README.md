# Overview

Test Deploy

This project is an example implementation connecting backend including protected routes. It is assumed backend is running locally on port `4000`. `PrivateRoute` is a component to allow for better understanding of a wrapped router.

# To Run Project

npm run start

# To Build and Deploy to GCP

($env:REACT_APP_BACKEND="https://gitlab-deploy-wbwty5b5hq-nn.a.run.app") -and (npm run build)

# Deployment

Dockerfile
Docker Compose
GCP Cloud Run

Enable Cloud Run API
Enable Google Container Registry API
Enable Compute Engine API

GItlab CI/CD Variables - BUCKET_NAME (name of GCP bucket) - REACT_APP_BACKEND - SERVICE_KEY_FILE

GCP mySQL

# Live Website

    http://rob.rob-wils.me/
