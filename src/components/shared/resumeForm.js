import React, { useState, useEffect  } from 'react';
import { Button, Form, Col, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { useHistory } from "react-router-dom";


const UpdateResumeForm = (props) => {
        const id = props.id
        const [resume, setResume] = useState({ employerName: "", jobTitle: "", year: ""  });
        // const [form, setForm] = useState({ display: "none" });
        
        const history = useHistory();

        useEffect(() => {
            async function fetchData() {
            const res = await fetch(`http://localhost:4001/api/resume/${id}`);
            res.json().then((res) => setResume(res));
            }
            fetchData();
        }, [id]);

 const handleChange = (event) => {
    setResume((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };
       
        // const handleEdit = (event, cat) => {
        //     event.preventDefault();
        //     console.log("rob is here again");
        //     console.log(resume);
        //     setForm({ display: "block" });
        //     setResume(resume);
        // };


        const handleSubmit = (event) => {
            event.preventDefault();
            console.log(id);
            fetch(`/api/resume/${id}`, {
            method: "PUT",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },

            //make sure to serialize your JSON body
            body: JSON.stringify(resume),
            }).then((response) => response.json());
            history.push("/");
        };

    


    return (

       
        <Form onSubmit={handleSubmit}>
            <FormGroup>
                <Label for="employerName">Employer Name</Label>
                <Input
                    type="text"
                    name="name"
                    id="name"
                    value={resume.employerName}
                    onChange={handleChange}
                />
            </FormGroup>
            <FormGroup>
                <Label for="jobTitle">Job Title</Label>
                <Input
                    type="text"
                    name="jobTitle"
                    id="jobTitle"
                    value={resume.jobTitle}
                    onChange={handleChange}
                />
            </FormGroup>
            <FormGroup>
                <Label for="year">Year</Label>
                <Input
                    type="text"
                    name="password"
                    id="password"
                    value={resume.year}
                    onChange={handleChange}
                />
            </FormGroup>
           
                     
            <FormGroup check row>
                <Col sm={{ size: 10, offset: 2 }}>
                <div className="button"><Button color="success">Submit</Button></div>
                </Col>
            </FormGroup>
        </Form>


    )

}

export default UpdateResumeForm

