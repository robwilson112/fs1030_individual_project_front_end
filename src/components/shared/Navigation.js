import React, { useState } from 'react'
import { Collapse, Navbar, NavbarToggler, Nav, NavItem, NavLink, Button} from 'reactstrap'
import { NavLink as RouteLink } from 'react-router-dom'
import '../../../src/App'
import rob from '../../assets/images/rob.png'
import fb from '../../assets/images/facebook (2).svg'
import ig from '../../assets/images/instagram (2).svg'
import tw from '../../assets/images/twitter (1).svg'
import home from '../../assets/images/home (1).png'
import file from '../../assets/images/file.svg'
import laptop from '../../assets/images/laptop.svg'
import envelope from '../../assets/images/envelope.svg'
import { useLocation, useHistory } from "react-router-dom";

const Navigation = () => {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = () => setIsOpen(!isOpen);
    const location = useLocation();
    const history = useHistory();
    


    const permission = sessionStorage.getItem("token");

    const logout = () => {
    history.push("/");
    sessionStorage.removeItem("token");
    
  };

    console.log(permission)

    const isAdmin = permission !== null 
    
    return (
       
     <header>
        <nav >
                  
        <Navbar className="sideBarNav">
            <div className="imgContainer">
                <img className="robImg" src={rob}></img>
                <h1 className="navHeader1">Rob Wilson 😎</h1>
                <h2 className="navHeader2"> <span className="aspiring">(Aspiring)</span> Web Developer</h2>
            </div>
            <div className="socialMediaContainer">
                <a className="socialMediaLink" href="https://www.facebook.com/" target="_blank"><img className="socialMedImage" src={fb} alt="Connect on Facebook" width={30} /></a>
                <a className="socialMediaLink" href="https://www.instagram.com/?hl=en" target="_blank"><img className="socialMedImage" src={ig} alt="Connect on Instagram" width={30} /></a>
                <a className="socialMediaLink" href="https://twitter.com/?lang=en" target="_blank"><img className="socialMedImage" src={tw} alt="Connect on Twitter" width={30} /></a>
            </div>
            <hr className="navHr"></hr>
                    <Nav>
                    <table className="linksTable">
                    <tr>
                    <NavItem className="navLinks" >
                        <td><NavLink style={{color:'honeydew'}} className="navText" tag={RouteLink} to="/"><img className="navImg" src={home}></img>Home</NavLink></td>
                    </NavItem>
                    </tr>
                    <tr>
                    <NavItem className="navLinks">
                       <NavLink style={{color:'honeydew'}} className="navText" tag={RouteLink} to="/contact"><img className="navImg" src={envelope}></img>Contact Me</NavLink>
                    </NavItem>
                    </tr>
                    {!isAdmin && ( 
                     <div>
                     <NavbarToggler onClick={toggle}/>
                     <tr> 
                    <Collapse isOpen={!isOpen}>   
                    <NavItem className="navLinks">
                        <NavLink style={{color:'honeydew'}}  className="navText" tag={RouteLink} to="/resume"><img className="navImg" src={file}></img>Resume</NavLink>
                    </NavItem>
                    
                    <NavItem className="navLinks">
                        <NavLink style={{color:'honeydew'}} className="navText" tag={RouteLink} to="/projects"><img className="navImg" src={file}></img>Projects</NavLink>
                    </NavItem> 
                    <NavItem   className="navLinks">
                        <NavLink style={{color:'honeydew'}} className="navText" tag={RouteLink} to="/login"><img className="navImg" src={laptop}></img>Login</NavLink>
                    </NavItem>
                    </Collapse> 
                    </tr> </div>  )}                     

                    {isAdmin && (     
                    <div>
                    <Collapse isOpen={!isOpen}>
                    <NavItem className="navLinks">
                       <NavLink style={{color:'goldenrod'}} className="navText" tag={RouteLink} to="/resumeAdmin"><img className="navImg" src={envelope}></img>Resume Admin</NavLink>
                    </NavItem>
                    
                    <NavItem   className="navLinks">
                        <NavLink style={{color:'goldenrod'}} className="navText" tag={RouteLink} to="/projectsAdmin"><img className="navImg" src={laptop}></img>Projects Admin</NavLink>
                    </NavItem>
                    <Button className="logout" color="danger" onClick={logout}>
                            Logout
                    </Button>
                                   
                    </Collapse>
                    </div>   )}   

                    </table>
                </Nav>
            
                <footer className="footerContent">&copy; 2021 Rob Wilson </footer>
        </Navbar>
          
         
      </nav>
      
    </header>
    
  );
    
}

export default Navigation