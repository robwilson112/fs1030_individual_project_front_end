import React, { useState } from 'react';
import { Button,  Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import UpdateResumeForm from './resumeForm';

const ModalForm = (props) => {
 
    const {
    buttonLabel,
    className
  } = props;

  const [modal, setModal] = useState(false);

//   
  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button color="danger" onClick={toggle}>Update</Button>
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>Update Resume</ModalHeader>
        
        <ModalBody> 
          
          <UpdateResumeForm/> 
        
        </ModalBody>

       
      </Modal>
    </div>
  );
}

export default ModalForm;