import React, { useState } from 'react'
import { Col, Row, Button, Form, FormGroup, Label, Input, Card, CardBody, CardText } from 'reactstrap'
import { useHistory, useLocation } from 'react-router-dom'

const Login = () => {
    let history = useHistory();
    let location = useLocation();
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [auth, setAuth] = useState(true)
// test
    const loginSubmit = async event => {
        event.preventDefault()
        const response = await fetch(`http://${process.env.REACT_APP_BACKEND}/users/auth`, {
            mode: 'cors',
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({email, password})
        })
        const payload = await response.json()
        if (response.status >= 400) {
            setAuth(false)
        } else {
            sessionStorage.setItem('token', payload.token)

            let { from } = location.state || { from: { pathname: "/resumeAdmin" } };
            history.replace(from);
        }
    }

    return (
        <main className="containerMainLong">
          <div className="contentMainLong">
        {!auth && 
            <Card className="text-white bg-primary my-5 py-4 text-center" className="formCardErr">
            <CardBody>
                <CardText className="text-white m-0" className="formCardTextErr"> &#10071; Invalid credentials, please try again &#10071;</CardText>
            </CardBody>
        </Card>
        }

            <Card className="text-white bg-primary my-5 py-4 text-center" className="formCard">
              <CardBody>
                  <CardText className="text-white m-0" className="formCardText">Enter credentials below</CardText>
              </CardBody>
          </Card>

        <hr className="mainHr" />
        <Form className="my-5" className="contactForm" onSubmit={loginSubmit}>
          
          <Row form>
            <Col md={6}>
              <FormGroup>
                <Label for="usernameEntry"></Label>
                <Input type="text" className="field" name="username" id="usernameEntry" placeholder="Username" bsSize="lg" value={email} onChange={e => setEmail(e.target.value)}/>
              </FormGroup>
            </Col>
            </Row>
            <Row>
            <Col md={6}>
              <FormGroup>
                <Label for="passwordEntry"></Label>
                <Input type="password" className="field" name="password" id="passwordEntry" placeholder="Valid password" bsSize="lg" onChange={e => setPassword(e.target.value)}/>
              </FormGroup>
            </Col>
          </Row>
          <Button className="loginButton" color="warning"><strong>Sign in</strong></Button>
          
        </Form>
        <div className="adminLoginText"> administrator login <span className="loginEmoji">&#128225;</span> </div>
        </div>
        </main>
    )
}

export default Login