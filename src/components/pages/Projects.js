import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from "react-router-dom";
import CarouselComp from '../shared/carousel.js'
import { Modal, Form, Button, ModalHeader, ModalBody, ModalForm, CardText, Card, CardBody } from 'reactstrap'


const Projects = (props) => {
        
    const [resumes, setResumes] = useState([]); // sets page
    const [form, setForm] = useState({ display: "none" });
    const [addForm, setAddForm] = useState({ display: "block" });
    const [item, setItem] = useState({ projectName: "", projectDesc: "", year: "" });
    const [editModal, setEditModal] = useState(false);
    const [modal, setModal] = useState(false);
    const history = useHistory();
     
   const toggleEdit = () => setEditModal(!editModal);
   const toggleNew = () => setModal(!modal);

    useEffect( ()=> {

      async function fetchData() {
          const res = await fetch(`http://${process.env.BACKEND}/project/portfolio`);
          res
          .json()
          .then((res) => setResumes(res))
          .catch((err) => console.log(err))
      }
      fetchData();
  }, []) // the empty array makes code run once

    const handleEdit = (event, item) => {
        console.log(event);
        console.log(item);
        event.preventDefault();
        setForm({ display: "block" });
        setEditModal({toggleEdit})
        setItem(item);
    };

     const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setItem((prevState) => ({
        ...prevState,
         [name]: value
         
        }));
    };    
  
   
    const handleUpdate = (event, item) => {
        event.preventDefault();
        fetch(`/project/portfolio/${item.projectId}`, {
        method: "put",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
        
            body: JSON.stringify(item),
            }).then((response) => response.json());
            history.go(0);
    };


    const handleAdd = () => {
           
        fetch("/project/portfolio", {
        method: "post",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },

        body: JSON.stringify(item),
        }).then((response) => response.json());
        history.go(0);   
  };

   

   const handleDelete = (event, item) => {
            event.preventDefault();
            
            fetch(`/project/portfolio/${item.projectId}`, {
            method: "delete",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            }).then((response) => response.json());
            history.go(0);
        };
            


    return (
        <main className="containerMainLong">

        
        <div className="contentMainLong">
        
        <div className="containerCarousel">
            <CarouselComp />      
        </div>

        <div className="resume">   
        
        <div className="resumeSubContainer">
        <Card className="text-white bg-secondary my-5 py-4 text-center" className="formCard">
            <CardBody>
          <CardText className="resumeHeader" className="formCardText" >  Projects</CardText>
          </CardBody>
        </Card>
          <hr className="mainHr" /> 

        {/* <Button className="dbButton"  color="warning" onClick={toggleNew}>Add New</Button> */}


        <Modal isOpen={modal} toggle={toggleNew}> 
        <h4>Add Project</h4>
        <ModalBody>
        <Form onSubmit={handleAdd} style={addForm}>
                        <div>
                        <label>
                            Project Name:
                            <input 
                            type="text"
                            name="projectName"
                            value={item.projectName}
                            onChange={handleChange}
                            />
                        </label>
                        </div>
                        <div>
                        <label>
                            Project Description:
                            <input
                            type="text"
                            name="projectDesc"
                            value={item.projectDesc}
                            onChange={handleChange}
                            />
                        </label>
                        <div>
                        <label>
                            Year:
                            <input
                            type="text"
                            name="year"
                            value={item.year}
                            onChange={handleChange}
                            />
                        </label>
                        </div>
                        </div>
                        <input type="submit" value="Submit" />
                        </Form>
                        </ModalBody>
                </Modal>        

        <Modal isOpen={editModal} toggle={toggleEdit}> 
        <h4>Edit Project</h4>
        <ModalBody>
        <Form onSubmit={(event) => handleUpdate(event, item)} style={form}>
                        <div>
                        <label>
                            Project Name:
                            <input
                            type="text"
                            name="projectName"
                            value={item.projectName}
                            onChange={handleChange}
                            />
                        </label>
                        </div>
                        <div>
                        <label>
                            Project Description:
                            <input
                            type="text"
                            name="projectDesc"
                            value={item.projectDesc}
                            onChange={handleChange}
                            />
                        </label>
                        <div>
                        <label>
                            Year:
                            <input
                            type="text"
                            name="year"
                            value={item.year}
                            onChange={handleChange}
                            />
                        </label>
                        </div>
                        </div>
                        <input type="submit" value="Submit" />
                        </Form>
                        </ModalBody>
                </Modal>

        
        {resumes.map((item) => ( 
                  // key gives each div an id of the cat
                  <div className="dbItem" key={item.projectId}> 
                  <p className="itemHeader">{item.projectName}</p>
                  <p>{item.projectDesc}</p>
                  <p>{item.year}</p>
                  {/* <Button className="dbButton" color="danger" onClick={(event) => {handleDelete(event, item);}}>Delete</Button>
                  <Button className="dbButton" color="info" onClick={(event) => {handleEdit(event, item);}}>Edit</Button> */}
                   
                  </div>  

        ))}


            
                   
        </div>
      </div>
    
        </div>
    </main>
    )
}

export default Projects