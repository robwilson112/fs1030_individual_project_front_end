import React from 'react'
import './App.css'
import Navigation from './components/shared/Navigation'
//import Footer from './components/shared/footer'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Home from './components/pages/Home'
import Contact from './components/pages/Contact'
import Projects from './components/pages/Projects'
import ProjectsAdmin from './components/pages/projectAdmin'
import Resume from './components/pages/Resume.js'
import Login from './components/pages/Login'
import ResumeAdmin from './components/pages/resumeAdmin'
import PrivateRoute from './components/shared/PrivateRoute'


function App() {
  return (
   <BrowserRouter>
        <Navigation />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/resume" component={Resume} />
          <Route exact path="/projects" component={Projects} />
          <Route exact path="/contact" component={Contact} />
          <Route exact path="/login" component={Login} />
          <PrivateRoute path="/resumeAdmin">
            <ResumeAdmin />
          </PrivateRoute>
          <PrivateRoute path="/projectsAdmin">
            <ProjectsAdmin />
          </PrivateRoute>
          
        </Switch>
    </BrowserRouter>
  )
}

export default App;
